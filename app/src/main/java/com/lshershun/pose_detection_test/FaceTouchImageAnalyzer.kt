package com.lshershun.pose_detection_test

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.ImageProxy
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.pose.Pose
import com.google.mlkit.vision.pose.PoseDetection
import com.google.mlkit.vision.pose.PoseDetectorOptions
import com.google.mlkit.vision.pose.PoseLandmark
import java.lang.ref.WeakReference
import kotlin.math.abs
import kotlin.math.max

@SuppressLint("UnsafeExperimentalUsageError")
class FaceTouchImageAnalyzer(val action: (Boolean) -> Unit) : ImageAnalysis.Analyzer {
    // Pose detection with streaming frames
    private val options = PoseDetectorOptions.Builder()
        .setDetectorMode(PoseDetectorOptions.STREAM_MODE)
        .setPerformanceMode(PoseDetectorOptions.PERFORMANCE_MODE_FAST)
        .build()

    private val poseDetector = PoseDetection.getClient(options)

    override fun analyze(imageProxy: ImageProxy) {
        val mediaImage = imageProxy.image
        if (mediaImage != null) {
            val image = InputImage.fromMediaImage(mediaImage, imageProxy.imageInfo.rotationDegrees)
            poseDetector.process(image)
                .addOnSuccessListener { pose ->
                    pose?.let {
                        detectFaceTouch(it)
                    }
                }
                .addOnFailureListener { e ->

                }
                .addOnCompleteListener {
                    imageProxy.close()
                }
        }
    }

    private fun detectFaceTouch(pose: Pose) {
        val leftEar = pose.getPoseLandmark(PoseLandmark.Type.LEFT_EAR)
        val rightEar = pose.getPoseLandmark(PoseLandmark.Type.RIGHT_EAR)
        val nose = pose.getPoseLandmark(PoseLandmark.Type.NOSE)

        val faceRadius = max(
            abs(leftEar.getX() - rightEar.getX()),
            abs(leftEar.getY() - rightEar.getY())
        ) / 2


        val leftHand = pose.getLeftHandLandMarks()
        val rightHand = pose.getRightHandLandMarks()

        val faceTouched =
            nose.isCloseTo(leftHand, faceRadius) || nose.isCloseTo(rightHand, faceRadius)

        action.invoke(faceTouched)
    }
}

private fun Pose.getLeftHandLandMarks(): List<PoseLandmark?> {
    return listOf(
        this.getPoseLandmark(PoseLandmark.Type.LEFT_INDEX),
        this.getPoseLandmark(PoseLandmark.Type.LEFT_THUMB),
        this.getPoseLandmark(PoseLandmark.Type.LEFT_WRIST),
        this.getPoseLandmark(PoseLandmark.Type.LEFT_PINKY)
    )
}

private fun Pose.getRightHandLandMarks(): List<PoseLandmark?> {
    return listOf(
        this.getPoseLandmark(PoseLandmark.Type.RIGHT_INDEX),
        this.getPoseLandmark(PoseLandmark.Type.RIGHT_THUMB),
        this.getPoseLandmark(PoseLandmark.Type.RIGHT_WRIST),
        this.getPoseLandmark(PoseLandmark.Type.RIGHT_PINKY)
    )
}

private fun PoseLandmark?.isCloseTo(other: List<PoseLandmark?>, delta: Float = 30f): Boolean {
    return other.any { it?.isCloseTo(this, delta) ?: false }
}

private fun PoseLandmark.isCloseTo(other: PoseLandmark?, delta: Float = 30f): Boolean {
    return if (other != null) {
        abs(this.position.x - other.position.x) < delta && abs(this.position.y - other.position.y) < delta
    } else false
}

private fun PoseLandmark?.getX() = this?.position?.x ?: 0f
private fun PoseLandmark?.getY() = this?.position?.y ?: 0f
